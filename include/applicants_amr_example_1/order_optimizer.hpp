#pragma once

#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include "applicants_amr_example_1/msg/next_order.hpp"

namespace applicants_amr_example_1
{

  class OrderOptimizer : public rclcpp::Node
  {
  private:
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr sub_current_position_;
    rclcpp::Subscription<applicants_amr_example_1::msg::NextOrder>::SharedPtr sub_next_order_;

  public:
    OrderOptimizer();
    ~OrderOptimizer() = default;

    void position_callback(const geometry_msgs::msg::PoseStamped::SharedPtr msg) const;
    void order_callback(const applicants_amr_example_1::msg::NextOrder::SharedPtr msg) const;
  };

} // namespace OrderOptimizer


