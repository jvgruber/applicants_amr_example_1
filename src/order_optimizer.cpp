#include <memory>

#include "applicants_amr_example_1/order_optimizer.hpp"

using std::placeholders::_1;

namespace applicants_amr_example_1
{

  OrderOptimizer::OrderOptimizer()
  : Node("OrderOptimizer")
  {
    sub_current_position_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
      "currentPosition", 10, std::bind(&OrderOptimizer::position_callback, this, _1));
    sub_next_order_ = this->create_subscription<applicants_amr_example_1::msg::NextOrder>(
      "nextOrder", 10, std::bind(&OrderOptimizer::order_callback, this, _1));
  }

  void OrderOptimizer::position_callback(const geometry_msgs::msg::PoseStamped::SharedPtr msg) const
  {
    RCLCPP_INFO(this->get_logger(), "I am at x, y, z: %f, %f, %f", msg->pose.position.x, msg->pose.position.y, 
      msg->pose.position.z);
  }
  
  void OrderOptimizer::order_callback(const applicants_amr_example_1::msg::NextOrder::SharedPtr msg) const
  {
    RCLCPP_INFO(this->get_logger(), "I got order: %d", msg->order_id);
  }

}

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<applicants_amr_example_1::OrderOptimizer>());
  rclcpp::shutdown();
  return 0;
}